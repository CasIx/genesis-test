# genesis-test

client - https://gitlab.com/CasIx/genesis-test-client \
core - https://gitlab.com/CasIx/genesis-test-core \
server - https://gitlab.com/CasIx/genesis-test-server \
gateway - https://gitlab.com/CasIx/genesis-test-gateway \


## Run
install mysql \
install node \
install nginx \
clone client \
run npm i && npm run start in client directory \
clone server \
change DB_USER and DB_PASSWORD to your config in .env file \
run npm i -g typescript \
run tsc in server directory \
run npm i && node dist/main.js in server directory \
clone gateway \
nginx -c {gateway_path}/nginx.conf \
open localhsot:9000
